## SkyScanner Search Api
A flask rest api with python 3.6


### Description:



### API specification

Address: defaul ip and port are kept in app/common/connstants.py
```
SERVER_IP = '127.0.0.1'
SERVER_PORT = 4000

[GET] http://127.0.0.1:4000/api/search

```

Input: input data is a querystring which holds one parametrers: number
```

example: http://127.0.0.1:4000/api/search?pageIndex=1&pageSize=100

```

Output: it is a json datastructure as below:
```
{"data":[],"success":true}
```

### Angular JS UI to test the api
This project also provides a UI built in Angularjs 1.3 to test the API.
```
http://127.0.0.1:4000/
```


### install
```
	python version 3.6
	
    virtualenv env/
    source env/bin/activate
    pip install -r requirements.txt
    python wsgi.py

```

### test
```
python skyScanner_tests.py
```

