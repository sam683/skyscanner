class Leg:
    def __init__(self):
        self.segments = []

    def to_json(self):
        return [item.to_json() for item in self.segments]
