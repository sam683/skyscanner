class Itinerary:
    def __init__(self):
        self.inbound_leg = None
        self.outbound_leg = None
        self.prices = []

    def to_json(self):
        res = dict(outbound_leg=self.outbound_leg.to_json(),
                   inbound_leg=self.inbound_leg.to_json())
        return {'itinerary': res}
