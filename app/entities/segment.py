class Segment:
    def __init__(self):
        self.departure_date = None,
        self.arrival_date = None,
        self.duration = 0
        self.flight_number = None
        self.stops = 0

    def to_json(self):
        return self.__dict__
