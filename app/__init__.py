from flask import Flask
from flask_restful import Api
from flask import render_template
from app.common.constants import SERVER_IP, SERVER_PORT
from app.api.search_api import SearchApi

from app.common.logger import Logger

app = Flask(__name__)

api = Api(app)
logger = Logger()
api.add_resource(SearchApi, '/api/search'
                 , resource_class_kwargs={'logger': logger})


@app.route('/')
def index():
    return render_template('index/index.html', serverAddress='http://{0}:{1}'
                           .format(SERVER_IP, SERVER_PORT)), 200


@app.errorhandler(404)
def not_found(error):
    return render_template('404.html'), 404


@app.errorhandler(401)
def not_found(error):
    return render_template('401.html'), 401