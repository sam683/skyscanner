import requests
import time
import datetime

from app.common.constants import *
from app.common.exceptions import SkyScannerException
from app.common.validation_message import ValidationMessage
from app.entities.response_object import ResponseObject
from app.entities.itinerary import Itinerary
from app.entities.leg import Leg
from app.entities.segment import Segment


class SkyScannerGateway:
    session_id = None

    def __init__(self, logger=None):
        self.logger = logger

    def search(self, page_index, page_size):
        SkyScannerGateway.session_id = self._create_session()
        self.logger.log('session is created. session id: {0}'.format(SkyScannerGateway.session_id))
        url = '{0}?apikey={1}&pageIndex={2}&pageSize={3}'.format(SkyScannerGateway.session_id, SKY_SCANNER_APIKEY,
                                                                 page_index, page_size)
        self.logger.log('sky scanner get request url: {0}'.format(url))
        res = requests.get(url)
        if not len(res.content):
            time.sleep(0.5)
            return self.search(page_index, page_size)
        result = self.parse_input(res.json())
        response = ResponseObject(result=result)
        return response

    @staticmethod
    def _create_session():
        if SkyScannerGateway.session_id is not None:
            return SkyScannerGateway.session_id

        today = datetime.datetime.now().date()

        payload = {"apikey": SKY_SCANNER_APIKEY,
                   "cabinclass": "Economy",
                   "country": "UK",
                   "currency": "GBP",
                   "locale": "en-GB",
                   "locationSchema": "iata",
                   "originplace": "EDI",
                   "destinationplace": "LHR",
                   "outbounddate": SkyScannerGateway.next_weekday(today, 0),
                   "inbounddate": SkyScannerGateway.next_weekday(today, 1),
                   "adults": "1",
                   "children": "0",
                   "infants": "0"
                   }

        response = requests.post(url=SKY_SCANNER_BASE_URL, data=payload)
        if response.status_code == 201:
            SkyScannerGateway.session_id = response.headers['location']
            return SkyScannerGateway.session_id
        else:
            raise SkyScannerException(ValidationMessage.GeneralServerFail)

    def parse_input(self, json_data):
        response = []
        itineraries = json_data['Itineraries']
        for item in itineraries:
            itinerary = Itinerary()
            itinerary.inbound_leg = self.extract_leg_and_segments(item['InboundLegId'], json_data)
            itinerary.outbound_leg = self.extract_leg_and_segments(item['OutboundLegId'], json_data)
            response.append(itinerary.to_json())
        return response

    @staticmethod
    def extract_leg_and_segments(leg_id, json_data):
        leg = Leg()
        legs = json_data['Legs']
        segments = json_data['Segments']
        leg_data = [l for l in legs if l['Id'] == leg_id][0]

        segment_ids = leg_data['SegmentIds']
        for segment_id in segment_ids:
            seg = [s for s in segments if s['Id'] == segment_id][0]
            segment = Segment()
            segment.arrival_date = seg['ArrivalDateTime']
            segment.departure_date = seg['DepartureDateTime']
            segment.duration = seg['Duration']
            segment.flight_number = seg['FlightNumber']
            segment.stops = len(leg_data['Stops'])
            leg.segments.append(segment)

        return leg

    '''0 = Monday, 1=Tuesday, 2=Wednesday...'''

    @staticmethod
    def next_weekday(d, weekday):
        days_ahead = weekday - d.weekday()
        if days_ahead <= 0:  # Target day already happened this week
            days_ahead += 7
        return d + datetime.timedelta(days_ahead)
