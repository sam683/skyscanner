'use strict';

   var app = angular
		.module('sky.scanner.App', ['ui.router', 'ngResource', 'ngMessages'])
		.config(['$stateProvider', '$urlRouterProvider', function config($stateProvider, $urlRouterProvider) {
		    $urlRouterProvider.otherwise('/search');
		    $stateProvider
				.state('search', {
				    url: '/search',
				    templateUrl: '/static/index/app/search/search.html',
				    controller: 'SearchCtrl',
				    displayName: 'search api'
				});
   }]);