'use strict';

angular
    .module('sky.scanner.App')
	.controller('SearchCtrl', ['$rootScope', '$scope','SearchService',
        function SearchCtrl($rootScope, $scope, SearchService) {

        $scope.pagedItems = [];
        $scope.loading = true;

        function fetch(page_index){
            SearchService.get({'pageIndex':page_index, 'pageSize':100}, function(response, headers){
            var data = response.data;
            if(data.length>0){
            for(var i=0; i< data.length; i++){
            var itinerary = data[i]['itinerary'];
            var outboundLeg = itinerary['outbound_leg'][0];
            var inboundLeg = itinerary['inbound_leg'][0];
            $scope.pagedItems.push({'out_departure_date' : outboundLeg.departure_date, 'out_arrival_date' : outboundLeg.arrival_date, 'out_duration': outboundLeg.duration,
             'out_stops': outboundLeg.stops, 'out_flight_number' : outboundLeg.flight_number, 'in_departure_date' : inboundLeg .departure_date, 'in_arrival_date' : inboundLeg.arrival_date, 'in_duration': inboundLeg.duration,
             'in_stops': inboundLeg .stops, 'in_flight_number' : inboundLeg .flight_number});
            }
            fetch(page_index+1);
            }
            else{
               $scope.loading = false;
               return false;
            }});
    }
    fetch(0);
}]);