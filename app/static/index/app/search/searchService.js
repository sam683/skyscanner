'use strict';

angular
    .module('sky.scanner.App')
    .service('SearchService', ['$resource', function ($resource) {
        return $resource("/api/search");
    }]);
