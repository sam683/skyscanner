from flask import jsonify
from flask import request
from flask_restful import Resource

from app.businesslogic.sky_scanner_gateway import SkyScannerGateway
from app.common.exceptions import SkyScannerException


class SearchApi(Resource):
    def __init__(self, logger=None):
        self.logger = logger

    '''
    accepts page index and page size from ui and
    returns flight data
    '''

    def get(self):
        try:

            # querystring args
            page_index = request.args.get('pageIndex')
            if page_index is None:
                page_index = eval(request.args.keys().__next__()).get('pageIndex', 1)

            page_size = request.args.get('pageSize')
            if page_size is None:
                page_size = eval(request.args.keys().__next__()).get('pageSize', 100)

            response = SkyScannerGateway(logger=self.logger).search(int(page_index), int(page_size))
            return jsonify({'success': True, 'data': response.result})
        except SkyScannerException as ex:
            return jsonify({'success': False, 'errors': [ex.message]})
        except Exception as ex:
            return jsonify({'success': False, 'errors': [ex.message]})
