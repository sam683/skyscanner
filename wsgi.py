from app import app as application
from app.common.constants import SERVER_IP, SERVER_PORT
if __name__ == "__main__":
    application.run(host=SERVER_IP, port=SERVER_PORT)
